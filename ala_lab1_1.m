word = ['к' 'в' 'а' 'д' 'р' 'о' 'к' 'о' 'п' 'т' 'е' 'р'];
alphabet = unique(word);

K_2 = [1 2;
       3 4];

K_3 = [1 2 5;
       2 5 7;
       7 8 4];

K_4 = [6 2 3 4;
       5 6 7 8;
       9 5 3 5;
       4 5 6 7];

keys_amount = 3;

W=m_write_word(alphabet,word);

w_c=char(zeros(keys_amount,length(word)));
w_d=char(zeros(keys_amount,length(word)));

k_counter = 0;
for K = {K_2,K_3,K_4}
    K=cell2mat(K);
    k_counter = k_counter+1;

    C=my_h_cipher(K,alphabet,W,'cipher');

    w_c(k_counter,:)=m_read_word(alphabet,C);

    rand_chars=randperm(length(alphabet),3);
    rand_pos=randperm(length(W),3);
    w_c(k_counter,rand_pos)=alphabet(rand_chars);
    
    P=my_h_cipher(K,alphabet,m_write_word(alphabet,w_c(k_counter,:)),'decipher');
    w_d(k_counter,:)=m_read_word(alphabet,P);
end

disp('given word:')
disp(word)
disp(newline)
disp('ciphered word for K_2, K_3 and K_4:')
disp(w_c)
disp(newline)
disp('deciphered word for K_2, K_3 and K_4: (after 3 random errors in each)')
disp(w_d)

function C = my_h_cipher(K,alphabet,W,mode)
    asize=length(alphabet);

    if strcmp(mode,'decipher')
        K=m_mod_inv(K,asize);
    elseif ~strcmp(mode,'cipher')
        error('mode must either ''cipher'' or ''decipher''');
    end

    wsize = length(W);
    ksize = size(K);
    ksize = ksize(1);
    C=zeros(1,wsize);

    for i=1:wsize/ksize
        C(ksize*(i-1)+1:ksize*i) = mod(K*W(ksize*(i-1)+1:ksize*i)',asize);
    end

end

function R = m_mod_inv(A, m)
    d=det(A);
    A=d*inv(A);
    d=round(mod(d,m));
    n=1;
    while mod(m*n+1,d)~=0 && n<10
        n=n+1;
    end
    d_inv = (m*n+1)/d;
    
    R = round(mod(d_inv*A,m));

end

function W = m_write_word(alphabet,word)
    wsize = length(word);
    W=zeros(1,wsize);
    for i=1:wsize
        W(i)=strfind(alphabet,word(i))-1;
    end
end

function word=m_read_word(alphabet,W)
    wsize = size(W);
    wsize=wsize(1)*wsize(2);
    W=reshape(W',1,wsize);
    word=blanks(wsize);
    for i=1:wsize
        word(i)=alphabet(W(i)+1);
    end
end
