word1 = ['к' 'в' 'а' 'д' 'р' 'о' 'к' 'о' 'п' 'т' 'е' 'р'];
word2 = ['о' 'б' 'м' 'а' 'н' 'м' 'а' 'н' 'у' 'л' 'о' 'в'];
alphabet = unique(strcat(word1,word2));

K = [1 7;
     3 4];

W1=m_write_word(alphabet,word1);
W2=m_write_word(alphabet,word2);

C1=my_h_cipher(K,alphabet,W1,'cipher');
C2=my_h_cipher(K,alphabet,W2,'cipher');

word1_c = m_read_word(alphabet,C1);
word2_c = m_read_word(alphabet,C2);

P1=my_h_cipher(K,alphabet,C1,'decipher');

disp('word1 and word2 ciphered:')
disp(word1_c)
disp(word2_c)
disp(newline)
disp('word1 deciphered:')
disp(word1)

C=m_write_word(alphabet,word1_c);
P=m_write_word(alphabet,word1);

C=[C(1:2);C(3:4)]';
P=[P(1:2);P(3:4)]';

KK=mod(C*m_mod_inv(P,length(alphabet)),length(alphabet));
disp(newline)
disp('Key found:')
disp(KK)
disp('True key:')
disp(K)
disp(newline)
disp('word2 deciphered:')
disp(m_read_word(alphabet,my_h_cipher(K,alphabet,C2,'decipher')))


function C = my_h_cipher(K,alphabet,W,mode)
    asize=length(alphabet);

    if strcmp(mode,'decipher')
        K=m_mod_inv(K,asize);
    elseif ~strcmp(mode,'cipher')
        error('mode must either ''cipher'' or ''decipher''');
    end

    wsize = length(W);
    ksize = size(K);
    ksize = ksize(1);
    C=zeros(1,wsize);

    for i=1:wsize/ksize
        C(ksize*(i-1)+1:ksize*i) = mod(K*W(ksize*(i-1)+1:ksize*i)',asize);
    end

end

function R = m_mod_inv(A, m)
    d=det(A);
    A=d*inv(A);
    d=round(mod(d,m));
    n=1;
    while mod(m*n+1,d)~=0 && n<10
        n=n+1;
    end
    d_inv = (m*n+1)/d;
    
    R = round(mod(d_inv*A,m));

end

function W = m_write_word(alphabet,word)
    wsize = length(word);
    W=zeros(1,wsize);
    for i=1:wsize
        W(i)=strfind(alphabet,word(i))-1;
    end
end

function word=m_read_word(alphabet,W)
    wsize = size(W);
    wsize=wsize(1)*wsize(2);
    W=reshape(W',1,wsize);
    word=blanks(wsize);
    for i=1:wsize
        word(i)=alphabet(W(i)+1);
    end
end
