alphabet='абвгдежзийклмнопрстуфхцчшчъыьэюя';
aloglen = ceil(log2(length(alphabet)));
word = 'мост'; 

n=7;
k=4;

A=ones(k,n-k)-vertcat(fliplr(eye(n-k)),zeros(2*k-n,n-k));
G=horzcat(eye(k),A);
H=horzcat(A',eye(n-k));
disp('G matrix')
disp(G)
disp ('H matrix')
disp(H)

M=m_write_word(alphabet,word);
M_c=zeros(1,length(M));
M_d=zeros(1,length(M));

%%шифровка
for j=1:length(M)/k
    M_c((j-1)*n+1:n*j)=mod(M((j-1)*k+1:k*j)*G,2);
end

disp('given word:')
disp(m_read_word(alphabet,M))
fprintf('\n');
disp('deciphered word with errors (fixed if possible):')

M_c_old = M_c;
for err_amount = 1:20
    M_c=M_c_old;
    %%добавление ошибок
    r=randperm(length(M_c),err_amount);
    M_c(r)=mod(M_c(r)+1,2);
    dflag=0;
    eflag=0;
    for j=1:length(M)/k
        %%дешифровка
        c=M_c((j-1)*n+1:n*j);
        s=mod(H*c',2);
        %%обнаружение одинарной ошибки
        err_bit =-1;
        for i = 1:n
            if H(:,i)==s
                err_bit=i;
            end
        end
        
        %%исправление одинарной ошибки
        if err_bit~=-1
            eflag=1;
            c(err_bit)=mod(c(err_bit)+1,2);
            s=mod(H*c',2);
        end

        %%обнаржуение двойной ошибки
        if c(k+1:n)~=s
            dflag=1;
        end
        
        M_d((j-1)*k+1:k*j)=c(1:k);
    end
    if dflag
        disp(m_read_word(alphabet,M_d) + " - "+num2str(err_amount)+" error bits [double error detected]")
    elseif eflag
        disp(m_read_word(alphabet,M_d) + " - "+num2str(err_amount)+" error bits [single error detected, fixed]")
    else
        disp(m_read_word(alphabet,M_d) + " - "+num2str(err_amount)+" error bits [no errors detected]")
    end
end

function W = m_write_word(alphabet,word)
    aloglen = ceil(log2(length(alphabet)));
    wsize = length(word)*aloglen;
    W=zeros(1,wsize);
    for i=1:length(word)
        W(aloglen*(i-1)+1:aloglen*i)= dec2bin(strfind(alphabet,word(i))-1,aloglen) - '0';
    end
end

function word=m_read_word(alphabet,W)
    aloglen = ceil(log2(length(alphabet)));
    wsize = size(W);
    wsize=wsize(1)*wsize(2);
    W=reshape(W',1,wsize);
    word=blanks(wsize/aloglen);
    for i=1:wsize/aloglen
        word(i)=alphabet(bin2dec(replace(num2str(W(aloglen*(i-1)+1:aloglen*i)),' ',''))+1);
    end
end

%{
%рабочий код извлечения из сообщения битов с сообщение, но при другом
расположении проверочных битов, написал и понял, что здесь все проще, но
пусть будет тут
posc = 3;
posm = 1;

for i=2:k+1
    m_d(posm:min(posm+2^(i-1)-2,length(m_d)))=c(posc:min(posc+2^(i-1)-2,length(c)));
    posm=posm+2^(i-1)-1;
    posc=posc+2^(i-1);
end
%}
